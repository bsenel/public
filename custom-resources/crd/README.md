# Example of CRD Usage

In this example, a CRD that concerns geolocations of nodes is designed and 
created.

## Configuration Files

YAML language is concrete part of CRD creation in Kubernetes. There are two 
YAML files that stick to different tasks which are creating custom resource 
definition of kind GeoLocation and an object of this kind. 

The "crd-example.yaml" file defines a custom resource with a kind of 
GeoLocation which includes "nodes", "coordinates", "city", "region", and 
"country" specs.

The "crd-obj-example.yaml" contains data which is compatible with requirements 
of GeoLocation custom resource.

## Steps

To create and check custom resource definition in Kubernetes

```
kubectl create -f crd-example.yaml
kubectl get crd
```

To create and check a geolocation object 

```
kubectl create -f crd-obj-example.yaml
kubectl get geolocations
kubectl describe geolocation ple-paris-nodes
```

## Other Notes

![image](./crd-example-cli.png)